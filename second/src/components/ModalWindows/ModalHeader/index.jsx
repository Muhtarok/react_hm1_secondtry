import PropTypes from "prop-types";

const ModalHeader = ({ children}) => {


    return (
      <h4 className="modalHeader">{children}</h4>
    )
  }
  
  ModalHeader.propTypes = {
    children: PropTypes.node.isRequired,
  };

export default ModalHeader;
