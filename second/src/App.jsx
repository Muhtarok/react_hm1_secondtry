import { useState } from "react";
import Button from "./components/Buttons";
import ModalWrapper from "./components/ModalWindows/ModalWrapper";

import "./App.css";
import "./App.scss";

function App() {
  const [isOpenFirstModal, setIsOpenFirstModal] = useState(false);
  const [isOpenSecondModal, setIsOpenSecondModal] = useState(false);

  const openFirstModalHandler = () => {
    setIsOpenFirstModal(!isOpenFirstModal);
  };

  const openSecondModalHandler = () => {
    setIsOpenSecondModal(!isOpenSecondModal);
  };

  return (
    <>
      <div className="buttonsContainer">
        <Button
          type="button"
          className="buttonPurple"
          onClick={openFirstModalHandler}
        >
          Open first modal
        </Button>

        <Button
          type="button"
          className="transparentBtn"
          onClick={openSecondModalHandler}
        >
          Open second modal
        </Button>
      </div>

      {isOpenFirstModal && (
        <ModalWrapper
          text="Product Delete"
          bodyText="By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted."
          className="modal"
          hasImage="true"
          footerCancelBtn="true"
          firstText="NO, CANCEL"
          footerDeleteBtn="true"
          secondText="YES, DELETE"
          footerAddToFavBtn="false"
          thirdText=""
          modalIsOpen={openFirstModalHandler}
        />
      )}

      {isOpenSecondModal && (
        <ModalWrapper
          text="Add product 'NAME'"
          bodyText="Description for you product"
          className="modal"
          hasImage="false"
          footerCancelBtn="false"
          footerDeleteBtn="false"
          footerAddToFavBtn="true"
          thirdText="ADD TO FAVORITE"
          modalIsOpen={openSecondModalHandler}
        />
      )}
    </>
  );
}

export default App;

